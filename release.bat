:: Generate a new Windows tails-installer release
:: Usage: `release 3.0.1`
:: Author: Luke Macken <lmacken@redhat.com>
echo Generating an exe of the tails-installer %1
rmdir /S /Q dist
rmdir /S /Q build
rmdir /S /Q tails-installer-%1

cd po
rmdir /S /Q locale
for %%f in (*.po) do (
    mkdir locale\%%~Nf\LC_MESSAGES
    python C:\Python27\Tools\i18n\msgfmt.py -o locale\%%~Nf\LC_MESSAGES\tails-installer.mo %%f
)
cd ..

python -OO setup.py py2exe

copy README.txt dist
copy data\fedora.ico dist\tails-installer.ico
copy data\vcredist_x86.exe dist\
copy data\tails-installer.nsi dist\tails-installer.nsi
"C:\Program Files\NSIS\makensis.exe" dist\tails-installer.nsi
rename dist tails-installer-%1
