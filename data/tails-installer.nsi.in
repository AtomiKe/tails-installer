;--------------------------------
;Tails Installer NSIS script

Name "Tails Installer 3.11.8"
OutFile "tails-installer-3.11.8-setup.exe"

SetCompressor lzma

!define LC_NSIS_INCLUDE_PATH "..\data"

;--------------------------------
;Include Modern UI
!include "MUI2.nsh"
;--------------------------------
;	Interface Configuration

  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "${LC_NSIS_INCLUDE_PATH}\tails-installer-nsi.bmp"
  !define MUI_ABORTWARNING

;--------------------------------
;	Pages
  !insertmacro MUI_PAGE_LICENSE "LICENSE.txt"
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;	Translations
@INSERT_TRANSLATIONS@

;--------------------------------
InstallDir "$PROGRAMFILES\Tails Installer"
InstallDirRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Tails Installer" ""

DirText $(s_InstallHere)

Icon tails-installer.ico

Section "Dummy Section" SecDummy

	; Install files.
	SetOverwrite on

	SetOutPath "$INSTDIR"
	File tails-installer.exe
	File LICENSE.txt
	File README.txt
	File MSVCP90.dll
	File MSVCP90.DLL
	File w9xpopen.exe
	File /r locale
	
	SetOutPath "$INSTDIR\tools"
	File tools\7z.dll
	File tools\7z.exe
	;File tools\7zCon.sfx
	File tools\7-Zip-License.txt
	File tools\dd.exe
	File tools\syslinux.exe
	
	; Create shortcut.
	SetOutPath -
	CreateDirectory "$SMPROGRAMS\Tails Installer"
	CreateShortCut "$SMPROGRAMS\Tails Installer\Tails Installer.lnk" "$INSTDIR\tails-installer.exe"
	CreateShortCut "$SMPROGRAMS\Tails Installer\Uninstall Tails Installer.lnk" "$INSTDIR\uninst.exe" "" "$INSTDIR\uninst.exe" 0

	; Optionally start program.
	MessageBox MB_YESNO|MB_ICONQUESTION $(s_RunNow) IDNO SkipRunProgram
	Exec "$INSTDIR\tails-installer.exe"
SkipRunProgram:

	; Create uninstaller.
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Tails Installer" "" "$INSTDIR"
	WriteRegStr HKEY_LOCAL_MACHINE "Software\Microsoft\Windows\CurrentVersion\Uninstall\Tails Installer" "DisplayName" "Tails Installer (remove only)"
	WriteRegStr HKEY_LOCAL_MACHINE "Software\Microsoft\Windows\CurrentVersion\Uninstall\Tails Installer" "UninstallString" '"$INSTDIR\uninst.exe"'
	WriteUninstaller "$INSTDIR\uninst.exe"

SectionEnd

UninstallText $(s_UnInstall)

Section Uninstall

	; Delete shortcuts.
	Delete "$SMPROGRAMS\Tails Installer\Tails Installer.lnk"
	Delete "$SMPROGRAMS\Tails Installer\Uninstall Tails Installer.lnk"
	RMDir "$SMPROGRAMS\Tails Installer"
	Delete "$DESKTOP\Tails Installer.lnk"

	; Delete registry keys.
	Delete "$INSTDIR\uninst.exe"
	DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Tails Installer"
	DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Tails Installer"

	; Delete files.
	Delete "$INSTDIR\tails-installer.exe"
	Delete "$INSTDIR\LICENSE.txt"
	Delete "$INSTDIR\README.txt"
	Delete "$INSTDIR\MSVCP90.DLL"
	Delete "$INSTDIR\MSVCP90.dll"
	Delete "$INSTDIR\w9xpopen.exe"
	
	Delete "$INSTDIR\tools\7z.dll"
	Delete "$INSTDIR\tools\7z.exe"
	;Delete "$INSTDIR\tools\7zCon.sfx"
	Delete "$INSTDIR\tools\7-Zip-License.txt"
	Delete "$INSTDIR\tools\dd.exe"
	Delete "$INSTDIR\tools\syslinux.exe"

	Delete "$INSTDIR\tails-installer.exe.log"

	; Remove the installation directories.
	RMDir "$INSTDIR\tools"
	RMDir "$INSTDIR"

SectionEnd
